# Apollo TS GraphQL base

I forked, I typed, I conquered.

Btw, you might want the [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin) & [Apollo GraphQL](https://marketplace.visualstudio.com/items?itemName=apollographql.vscode-apollo) extensions for VSCode.
