import { Book, BookCreateInput, BookUpdateInput, BookWhereInput } from '@prisma/client'
import { DataSource } from 'apollo-datasource'
import { withPrisma } from '../utils/dbMiddleware'

export class BooksProvider extends DataSource {
  public async getBook(args: BookWhereInput): Promise<Book> {
    return withPrisma(prisma => prisma.book.findFirst({ where: { ...args } }))
  }

  public async addBook(args): Promise<Book> {
    const { title, authorId, author } = args
    return withPrisma(prisma => prisma.book.create({
      data: {
        title: title,
        Person: authorId
        ? { connect: { id: authorId } }
        : { create: { firstName: author.firstName, lastName: author.lastName }} } 
    }))
  }

  public async getBooks(): Promise<Array<Book>> {
    return withPrisma(prisma => prisma.book.findMany({
      include: {Person: true}
    }))
  }

  public async updateBook(args: UpdateByIdArgs<BookUpdateInput>): Promise<Book> {
    const {id, ...updateParams} = args
    return withPrisma(prisma => prisma.book.update({
      where: { id },
      data: { ...updateParams }
    }))
  }
}