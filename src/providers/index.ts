import { BooksProvider } from "./book"
export { BooksProvider } from "./book"

export interface DataSources {
    booksProvider: BooksProvider
}

export const providers = {
    booksProvider: new BooksProvider()
}
