import { Book, BookCreateInput, BookUpdateInput, BookWhereInput } from "@prisma/client";
import { Context } from "src";

export const bookResolver = {
  Query: {
    getBook: (_: never, args: BookWhereInput, ctx: Context): Promise<Book> => ctx.dataSources.booksProvider.getBook(args),
    getBooks: (_: never, __: never, ctx: Context): Promise<Array<Book>> => ctx.dataSources.booksProvider.getBooks()
  },
  Mutation: {
    addBook: (_: never, args: BookCreateInput, ctx: Context): Promise<Book> => ctx.dataSources.booksProvider.addBook(args),
    updateBook: (_: never, args: UpdateByIdArgs<BookUpdateInput>, ctx: Context): Promise<Book> => ctx.dataSources.booksProvider.updateBook(args)
  }
}

