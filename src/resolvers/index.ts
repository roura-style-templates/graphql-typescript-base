import { bookResolver } from './book'

export const resolvers = {
    Query: {
        ...bookResolver.Query,
    },
    Mutation: {
        ...bookResolver.Mutation,
    }
}