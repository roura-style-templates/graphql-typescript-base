import { gql } from 'apollo-server'

export const bookType = gql`
    scalar DateTime

    type Book {
        id: Int
        createdAt: DateTime
        title: String!
        pagesAmount: Int
        author: Person
    }

    input AuthorInput {
        firstName: String!
        lastName: String!
    }

    type Person {
        id: Int
        firstName: String!
        lastName: String!
        publishedBooks: [Book]
    }

    type Query {
        getBook(id: Int!): Book
        getBooks: [Book]
    }

    type Mutation {
        addBook(title: String!, authorId: Int, author: AuthorInput): Book
        updateBook(id: Int!, title: String, author: String): Book
    }
`
