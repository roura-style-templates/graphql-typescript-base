import { mergeTypeDefs } from 'graphql-tools'
import { bookType } from './book'

export const typeDefs = mergeTypeDefs([
    bookType,
])