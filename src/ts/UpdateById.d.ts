type UpdateByIdArgs<T> = {
    id: number
} & T