import { PrismaClient } from "@prisma/client"

type DbCallback = (dbClient: PrismaClient) => any

export const withPrisma = async (callback: DbCallback) => {
    let result: any
    const prisma = new PrismaClient()
    try {
        result = await callback(prisma)
    } catch (error) {
        console.error(error)
        throw error
    } finally {
        await prisma.$disconnect()
    }
    return result
}
