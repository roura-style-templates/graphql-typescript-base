import { ApolloServer, IResolvers } from 'apollo-server'

import { typeDefs } from './ts'
import { BooksProvider, providers } from './providers'
import { resolvers } from './resolvers'

export interface Context {
  dataSources: {
    booksProvider: BooksProvider,
  }
}

const dataSources = (): Context['dataSources'] => providers

const server = new ApolloServer({
  typeDefs,
  resolvers: resolvers as IResolvers,
  dataSources,
})

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`) // tslint:disable-line no-console
})