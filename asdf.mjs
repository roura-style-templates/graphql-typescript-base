import { parse, prettify } from '@jedmao/prisma2-sdl'
import { readFileSync } from 'fs'

const ast = parse(readFileSync('schema.prisma'))
console.log(prettify(ast))